(function () {

    // Handling installation event to navigate user to extension page
    chrome.runtime.onInstalled.addListener(function (installationDetails) {
        switch (installationDetails.reason) {
            // on extension installed
            case "install": {
                VkTune.runtime.onInstalled();
                break;
            }
            // on extension updated
            case "update": {
                VkTune.runtime.onUpdated(installationDetails.previousVersion);
                break;
            }
            default: break;
        }
    });

    // Handling update available event
    chrome.runtime.onUpdateAvailable.addListener(function (details) {
        VkTune.runtime.onUpdateAvailable(details.version);
    });

    // Handling notifications click
    chrome.notifications.onClicked.addListener(function (notificationId) {
        switch (notificationId) {
            case "VkTune.updateAvailable": {
                chrome.runtime.openOptionsPage();
                break;
            }
            case "VkTune.updated": {
                chrome.tabs.create({ url: chrome.extension.getURL('pages/welcome.html') });
                break;
            }
            default: break;
        }
    });

    // Handling UI events to call addon's api
    chrome.runtime.onMessage.addListener(function (message) {
        switch (message.name) {
            // on audio download event
            case "audio.download": {
                VkTune.runtime.onAudioDownload(message.value);
                break;
            }
            // on mainAdsHide
            case "ads.mainAdsHide": {
                VkTune.runtime.onMainAdvertismentHidden(message.value);
                break;
            }
            // on promotedPostsHide
            case "ads.promotedPostsHide": {
                VkTune.runtime.onPromotedPostHide(message.value);
                break;
            }
            // on groupAdsHide
            case "ads.groupAdsHide": {
                VkTune.runtime.onGroupAdvertismentHide(message.value);
                break;
            }
            // on ga-init
            case "ga-init": {
                VkTune.analytics.initGA(message.value.id, message.value.lang);
                break;
            }
            // on error
            case "error": {
                VkTune.analytics.logError(message.value.action, message.value.label);
                var userText = message.value.userMessage;
                if (!userText) {
                    userText = message.value.label;
                }
                VkTune.notifications.error("error." + message.value.action.toLowerCase(), chrome.i18n.getMessage("extensionName"), userText);
                break;
            }            
            default: break;
        }
    });

})();