VkTune.settings.getAll() //get settings before app start
    .then(function (settings) { //and pass them as variable to app
        // Function that parses DOM to find UserID
        function getUserIdFromPageSources() {
            // try to get UserID from left-side links
            var getUserIdFromLeftSideLinks = function () {
                var uid = 0;
                var identifiedLink = document.querySelector("a.left_row[href^='/albums'], a.left_row[href^='/audios']");
                if (identifiedLink) {
                    uid = parseInt(identifiedLink.getAttribute("href").replace("/albums", "").replace("/audios", ""));
                }
                return uid;
            }
            // try to get UserID from id parameterized links
            var getUserIdFromParameterizedHrefAttrs = function () {
                var uid = 0;
                var parameterizedLink = document.querySelector("[href*='?id=']");
                if (parameterizedLink) {
                    var attrValue = parameterizedLink.getAttribute("href");
                    uid = parseInt(attrValue.match(/id=(\d*)/)[1]);
                }
                return uid;
            }
            // try to get UserID from vk_id parameterized links
            var getUserIdFromParameterizedSrcAttrs = function () {
                var uid = 0;
                var parameterizedLink = document.querySelector("[src*='vk_id=']");
                if (parameterizedLink) {
                    var attrValue = parameterizedLink.getAttribute("src");
                    uid = parseInt(attrValue.match(/vk_id=(\d*)/)[1]);
                }
                return uid;
            }
            // try to get UserID from user panel in header
            var getUserIdFromOwnerPanel = function () {
                var uid = 0;
                var ownerPanel = document.querySelector(".owner_panel[class*='al_u']");
                var ownerPanelRE = /al_u(\d+)/;
                if (ownerPanel && ownerPanelRE.test(ownerPanel.className)) {
                    uid = ownerPanel.className.match(ownerPanelRE)[1];
                }
                return uid;
            }
            // try to get UserID from page scripts
            var getUserIdFromPageScripts = function () {
                var uid = 0;
                var jsFilterProto = Array.prototype.filter,
                    pageScripts = document.querySelectorAll("script:not([src])"),
                    idRegex = /vk\s*=\s*[^}]*"*id"*:\s*"*(\d+)"*,/,
                    filteredScripts = jsFilterProto.call(pageScripts, function (s) { // try to find vk = {.. id: 000, ...}
                        return idRegex.test(s.innerText);
                    });

                if (filteredScripts.length) { //if we found userID in scripts
                    uid = parseInt(filteredScripts[0].innerText.match(idRegex)[1]);
                }
                return uid;
            }

            var uid = 0,
                searchProviders = [getUserIdFromLeftSideLinks,
                    getUserIdFromParameterizedHrefAttrs,
                    getUserIdFromParameterizedSrcAttrs,
                    getUserIdFromOwnerPanel,
                    getUserIdFromPageScripts],
                i = 0;

            while ((!uid || uid < 1) && i < searchProviders.length) {
                uid = searchProviders[i]();
                i++;
            }

            return uid;
        }
        
        // Function that called on all DOM changes
        function mutationCallback(mutationsList) {
            // if user allowed addon to inject download button in DOM, we should track all .audio_row elements then
            if (settings['audio.downloading']) {
                for (var i = 0; i < mutationsList.length; i++) {
                    var mutation = mutationsList[i];
                    for (var j = 0; j < mutation.addedNodes.length; j++) {
                        var addedNode = mutation.addedNodes[j];
                        if (addedNode.nodeType == 1) {

                            // in case of vk.com (desktop version)
                            if (VkTune.helpers.hasClass(addedNode, audioRowActions)) {
                                var relatedAudioRow = VkTune.helpers.selfOrParent(addedNode.parentElement, audioRowClass);
                                if (relatedAudioRow) {
                                    if (!VkTune.helpers.hasClass(relatedAudioRow, "audio_claimed")) { // if audio not locked by vk.com
                                        var audioFullId = relatedAudioRow.getAttribute(audioIdAttributeName);

                                        var btn = document.createElement("button");             // add button to document
                                        btn.className = btnClass + " " + "audio_row__action";   // set it's class for proper styling
                                        btn.setAttribute("title", chrome.i18n.getMessage("downloadButtonText")); // set button's title
                                        btn.setAttribute(audioIdAttributeName, audioFullId);    // set button's fullId attribute
                                        btn.setAttribute("onmouseover", "showTooltip(this, { text:'" + chrome.i18n.getMessage("downloadButtonText") + "', shift: [7,4,0], black: 1, needLeft: 1})");
                                        btn.setAttribute(audioIdAttributeName, audioFullId);    // set button's fullId attribute

                                        var idsToPreload = [];
                                        var ns = VkTune.helpers.nextSibling(relatedAudioRow, audioRowClass);
                                        while (idsToPreload.length < 4 && ns != null) {
                                            if (!VkTune.helpers.hasClass(ns, "audio_claimed")) {
                                                var nsAudioId = ns.getAttribute(audioIdAttributeName);
                                                if (nsAudioId) {
                                                    idsToPreload.push(nsAudioId);
                                                }
                                            }
                                            ns = VkTune.helpers.nextSibling(ns, audioRowClass);
                                        }

                                        if (settings['audio.calcBitrate']) {
                                            VkTune.downloadingProcessor.getAudioBitrate([audioFullId], idsToPreload, USER_ID)
                                                .then(function (data) {
                                                    var audioInfo = data.find(function (r) { return r.fullId == audioFullId });
                                                    if (audioInfo) {
                                                        var infoElement = VkTune.downloadingProcessor.getAudioInfoElement(audioInfo.bitrate, audioInfo.filesize);
                                                        btn.appendChild(infoElement);
                                                    }
                                                });
                                        }

                                        btn.onclick = function (e) {
                                            e.stopPropagation();

                                            var audioId = this.getAttribute(audioIdAttributeName);
                                            VkTune.downloadingProcessor.downloadAudios([audioId], idsToPreload, USER_ID)
                                                .then(function (downloadInfo) {
                                                    chrome.runtime.sendMessage({ name: "audio.download", value: downloadInfo });
                                                    btn.classList.add("downloaded");
                                                });
                                        }

                                        addedNode.appendChild(btn);                             // add downloading button to DOM
                                    }
                                }
                            }

                            // in case of m.vk.com (mobile version) - pretty similar to desktop version - just another wrappers to set proper styling
                            if (VkTune.helpers.hasClass(addedNode, "ai_menu") && VkTune.helpers.hasClass(addedNode, "wi_actions")) {
                                var audioPlayerBtn = addedNode.querySelector("div[onclick^='audioplayer']");
                                var downloadBtns = addedNode.querySelector("." + btnClass);

                                // if player exists and we didn't added buttons already
                                if (audioPlayerBtn && !downloadBtns) {
                                    // Get audio id from element
                                    var mobileArgumentParserRE = /audioplayer\.[^\(]*\('([^_]*_[^_]*)/i;
                                    var audioArgument = audioPlayerBtn.getAttribute("onclick");
                                    var audioArgumentMatches = audioArgument.match(mobileArgumentParserRE);

                                    if (audioArgumentMatches.length > 1) {
                                        var audioFullId = audioArgumentMatches[1];

                                        var btn = document.createElement("div");                // add button to document
                                        btn.className = btnClass + " " + "wia_item";            // set it's class for proper styling
                                        btn.setAttribute("title", chrome.i18n.getMessage("downloadButtonText"));  // set button's title
                                        btn.setAttribute(audioIdAttributeName, audioFullId);    // set button's fullId attribute
                                        var textContainer = document.createElement("span"); 
                                        textContainer.textContent = chrome.i18n.getMessage("downloadButtonText"); // set button's text
                                        btn.appendChild(textContainer);

                                        if (settings['audio.calcBitrate']) {
                                            VkTune.downloadingProcessor.getAudioBitrate([audioFullId], [], USER_ID)
                                                .then(function (data) {
                                                    var audioInfo = data.find(function (r) { return r.fullId == audioFullId });
                                                    if (audioInfo) {
                                                        var infoElement = VkTune.downloadingProcessor.getAudioInfoElement(audioInfo.bitrate, audioInfo.filesize);
                                                        btn.appendChild(infoElement);
                                                    }
                                                });
                                        }

                                        btn.onclick = function (e) {
                                            e.stopPropagation();

                                            var audioId = this.getAttribute(audioIdAttributeName);
                                            VkTune.downloadingProcessor.downloadAudios([audioId], [], USER_ID)
                                                .then(function (downloadInfo) {
                                                    chrome.runtime.sendMessage({ name: "audio.download", value: downloadInfo });
                                                });
                                        }

                                        addedNode.appendChild(btn);                     // add downloading button to DOM
                                    }
                                }
                            }
                        }
                    }
                }

                if (settings['audio.playlistsDownloading']) {
                    // playlists handling
                    var playlists = document.querySelectorAll(".audio_pl_snippet");
                    playlists.forEach(function (playlistElement) {
                        var downloadBtn = playlistElement.querySelector("." + btnClass);
                        if (!downloadBtn) {
                            var plId = playlistElement.getAttribute("data-playlist-id");
                            if (plId) {
                                var actionsWrapper = playlistElement.querySelector(".audio_pl_snippet__actions");
                                if (actionsWrapper) {
                                    var btn = document.createElement("div");
                                    btn.className = btnClass + " " + "audio_pl_snippet__action_btn";
                                    btn.setAttribute("title", chrome.i18n.getMessage("downloadPlaylistButtonText"));
                                    btn.setAttribute("onmouseover", "showTooltip(this, { text:'" + chrome.i18n.getMessage("downloadPlaylistButtonText") + "', shift: [7,4,0], black: 1, needLeft: 1})");

                                    btn.onclick = function () {
                                        VkTune.downloadingProcessor.downloadPlaylist(plId, USER_ID)
                                            .then(function (downloadInfo) {
                                                var albumName = prompt(chrome.i18n.getMessage("albumNamePromptTitle"), downloadInfo.title);
                                                if (albumName != null) {
                                                    downloadInfo.title = albumName;
                                                    chrome.runtime.sendMessage({ name: "audio.download", value: downloadInfo });
                                                }
                                                btn.classList.add("downloaded");
                                            });
                                    }

                                    var playBtn = actionsWrapper.querySelector(".audio_pl_snippet_play_small");
                                    if (playBtn) {
                                        actionsWrapper.insertBefore(btn, playBtn);
                                    } else {
                                        actionsWrapper.appendChild(btn);
                                    }
                                }
                            }
                        }
                    });

                    // wall posts lists handling
                    var wallAudioLists = document.querySelectorAll(".wall_audio_rows");
                    wallAudioLists.forEach(function (rowsWrapper) {
                        var audioRowsNumber = rowsWrapper.querySelectorAll("." + audioRowClass + ":not(.audio_claimed)").length;
                        var downloadBtn = rowsWrapper.querySelector("." + btnClass);
                        if (!downloadBtn && audioRowsNumber > 1) {
                            var btn = document.createElement("div");
                            btn.className = btnClass;
                            btn.setAttribute("title", chrome.i18n.getMessage("downloadPlaylistButtonText"));
                            btn.textContent = chrome.i18n.getMessage("downloadPlaylistButtonText");

                            btn.onclick = function (e) {
                                var audioRows = this.parentElement.querySelectorAll("." + audioRowClass + ":not(.audio_claimed)");
                                var audioIds = [];
                                for (var i = 0; i < audioRows.length; i++) {
                                    audioIds.push(audioRows[i].getAttribute(audioIdAttributeName));
                                }

                                VkTune.downloadingProcessor.downloadAudios(audioIds, [], USER_ID)
                                    .then(function (downloadInfo) {
                                        var albumName = prompt(chrome.i18n.getMessage("albumNamePromptTitle"), "");
                                        if (albumName != null) {
                                            downloadInfo.title = albumName;
                                            chrome.runtime.sendMessage({ name: "audio.download", value: downloadInfo });
                                        }
                                    });
                            }

                            rowsWrapper.appendChild(btn);
                        }
                    });
                }
            }

            // If user allowed to hide left-side advertisment block
            if (settings['ads.mainBlock'] != "show") {
                VkTune.contentProcessors['ads.mainBlock'](settings['ads.mainBlock']);
            }

            // If user allowed to hide promoted posts in his feed
            if (settings['ads.promotedPosts'] != 'show') {
                VkTune.contentProcessors['ads.promotedPosts'](settings['ads.promotedPosts']);
            }

            // If user allowed to hide promoted posts in his feed
            if (settings['ads.groupWallAds'] != 'show') {
                VkTune.contentProcessors['ads.groupWallAds'](settings['ads.groupWallAds']);
            }
        }

        var audioRowClass = "audio_row", 					 //to select item
            audioRowActions = "audio_row__actions",			 //to select actions wrapper
            btnClass = "audio_download",
            extensionStyleId = "vktune-download-btn-style",
            audioIdAttributeName = "data-full-id",
            mo = new MutationObserver(mutationCallback),
            USER_ID = getUserIdFromPageSources();

        // Build proper url with chrome.runtime.getURL
        if (!document.getElementById(extensionStyleId)) { // if style not added already
            var styleElement = document.createElement("style"); // element to insert into DOM
            styleElement.id = extensionStyleId; // indentifying style to prevent duplicates

            var downloadBtnStyle = ".audio_download.audio_row__action { background: url('" + chrome.runtime.getURL('images/arrow.png') + "') 3px center no-repeat; background-size: 16px; padding-left: 22px; min-width: 22px; width: auto; vertical-align: top; }" +
                                   ".audio_download.audio_row__action.downloaded { background-image: url('" + chrome.runtime.getURL('images/check.png') + "'); }" + 
                                   ".audio_download.audio_row__action span { display: block; }" + // button css style with dynamic path to image
                                   "._vktune_audio_info { font-size: 8px; text-align: right; line-heigth: 8px; }" +
                                   ".audio_download.wia_item { display: table!important; width: 100%!important; box-sizing: border-box; }" + 
                                   ".audio_download.wia_item > span { display: table-cell; }" + 
                                   ".audio_download.wia_item ._vktune_bitrate { padding-right: 3px; border-right: 1px solid; margin-right: 3px; }" + 
                                   ".audio_download.audio_pl_snippet__action_btn { background: url('" + chrome.runtime.getURL('images/arrow.png') + "') 3px center no-repeat; background-size: 16px; width: 16px; display: block; }" +  // playlists btn style
                                   ".wall_audio_rows > .audio_download { text-align: center; cursor: pointer; overflow: hidden; height: 0; border: 2px solid #f5f7fa; margin: 0 10px; border-radius: 3px; }" + 
                                   ".wall_audio_rows:hover > .audio_download { height: auto; background: #f5f7fa; line-height: 22px; margin: 0; }" + 
                                   ".wall_audio_rows > .audio_download:hover { background: #dae1e8; border-color: #dae1e8; }";
            var markedStyle = ".vktune_marked { opacity: 0.3; }";

            styleElement.innerText = downloadBtnStyle + markedStyle; // filling style element with generated css
            document.querySelector("head").appendChild(styleElement); // appending style to head
        }

        if (!USER_ID && !document.querySelector('#index_login, .new_form, #quick-login-form')) { // ID not detected and not login page
            // Unable to get user id
            var pageInfo = window.location.href;
            chrome.runtime.sendMessage({ name: "error", value: { action: "UserId", label: "Unable to receive user ID at " + pageInfo, userMessage: chrome.i18n.getMessage("errorMessageUnableToGetUserId") } });
        } else {
            var lang = document.querySelector("html").getAttribute("lang");
            chrome.runtime.sendMessage({ name: "ga-init", value: { id: USER_ID, lang: lang } });
        }

        // Using of DOM observer is important because of using AJAX by vk.com. 
        mo.observe(document.body, { 'childList': true, 'subtree': true });
    }, function () {
        // Error happened during settings request
        chrome.runtime.sendMessage({ name: "error", value: { action: "UserSettings", label: "Unable to load extension settings", userMessage: chrome.i18n.getMessage("errorMessageUnableToGetSettings") } });
    });