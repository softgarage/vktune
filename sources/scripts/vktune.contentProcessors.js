(function () {

    if (!window.VkTune)
        window.VkTune = {};

    VkTune.contentProcessors = (function () {
        function markElement(element, titleMessage) {
            element.className += " " + VkTune.constants.vktuneMarkedClass;
            element.setAttribute("title", titleMessage);
        }

        function processMainAdsBlocks(action) {
            if (action == VkTune.constants.hideActionKey || action == VkTune.constants.markActionKey) {
                var adsBlocks = document.querySelectorAll(VkTune.constants.leftAdsBlockSelector); //Select this block 
                var numberOfChanges = 0;
                for (var i = 0; i < adsBlocks.length; i++) {
                    if (action == VkTune.constants.hideActionKey) {
                        VkTune.helpers.removeNode(adsBlocks[i]); // and remove from DOM
                        numberOfChanges++;
                    }
                    if (action == VkTune.constants.markActionKey &&
                        !VkTune.helpers.hasClass(adsBlocks[i], VkTune.constants.vktuneMarkedClass)) {
                        markElement(adsBlocks[i], chrome.i18n.getMessage("adsMainBlockMarkedTitle"));
                        numberOfChanges++;
                    }
                }
                if (numberOfChanges) { // send message to background.js to track event if needed
                    chrome.runtime.sendMessage({ name: "ads.mainAdsHide", value: { method: action, count: numberOfChanges } });
                }
            }
        }

        function processPromotedPosts(action) {
            if (action == VkTune.constants.hideActionKey || action == VkTune.constants.markActionKey) {
                var promotedPosts = document.querySelectorAll(VkTune.constants.promotedPostsSelector); //select all posts
                var numberOfChanges = 0;
                for (var i = 0; i < promotedPosts.length; i++) {
                    if (action == VkTune.constants.hideActionKey) {
                        VkTune.helpers.removeNode(promotedPosts[i]); // remove from DOM
                        numberOfChanges++;
                    }
                    if (action == VkTune.constants.markActionKey &&
                        !VkTune.helpers.hasClass(promotedPosts[i], VkTune.constants.vktuneMarkedClass)) {
                        markElement(promotedPosts[i], chrome.i18n.getMessage("adsPromotedPostsMarkedTitle"));
                        numberOfChanges++;
                    }
                }
                if (numberOfChanges) { // send message to background.js to track event if needed
                    chrome.runtime.sendMessage({ name: "ads.promotedPostsHide", value: { method: action, count: numberOfChanges } });
                }
            }
        }

        function processGroupWallAds(action) {
            if (action == VkTune.constants.hideActionKey || action == VkTune.constants.markActionKey) {
                var paidPostIndicators = document.querySelectorAll(VkTune.constants.groupAdsSelector); //select all posts icons
                var numberOfChanges = 0;
                for (var i = 0; i < paidPostIndicators.length; i++) {
                    var paidPost = VkTune.helpers.selfOrParent(paidPostIndicators[i], VkTune.constants.postClass);
                    if (paidPost) {
                        if (action == VkTune.constants.hideActionKey) {
                            VkTune.helpers.removeNode(paidPost); // remove post from DOM
                            numberOfChanges++;
                        }
                        if (action == VkTune.constants.markActionKey &&
                            !VkTune.helpers.hasClass(paidPost, VkTune.constants.vktuneMarkedClass)) {
                            markElement(paidPost, chrome.i18n.getMessage("adsGroupAdsMarkedTitle"));
                            numberOfChanges++;
                        }
                    }
                }
                if (numberOfChanges) { // send message to background.js to track event if needed
                    chrome.runtime.sendMessage({ name: "ads.groupAdsHide", value: { method: action, count: numberOfChanges } });
                }
            }
        }
        
        return {
            'ads.mainBlock': processMainAdsBlocks,
            'ads.promotedPosts': processPromotedPosts,
            'ads.groupWallAds': processGroupWallAds
        }
    })();


    VkTune.downloadingProcessor = (function () {

        var audioInfo = [];

        function filterPreloadedAudios(audioIds) {
            return audioIds.filter(function (val) { return audioInfo[val] === undefined });
        }

        function preloadAudios(audioIds, userId) {
            function _decryptAudioData(audioDataObj, userId) {
                function _audio_unmask_source(t, vk_id) {
                    var s = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMN0PQRSTUVWXYZO123456789+/=";

                    function a(t) {
                        if (!t || t.length % 4 == 1)
                            return !1;
                        for (var e, i, o = 0, a = 0, r = ""; i = t.charAt(a++);)
                            i = s.indexOf(i),
                                ~i && (e = o % 4 ? 64 * e + i : i,
                                    o++ % 4) && (r += String.fromCharCode(255 & e >> (-2 * o & 6)));
                        return r
                    }
                    function z(t, e) {
                        var i = t.length,
                            o = [];
                        if (i) {
                            var a = i;
                            for (e = Math.abs(e); a--;)
                                e = (i * (a + 1) ^ e + a) % i,
                                    o[a] = e
                        }
                        return o
                    }
                    var r = {
                        v: function (t) {
                            return t.split("").reverse().join("")
                        },
                        r: function (t, e) {
                            t = t.split("");
                            for (var i, o = s + s, a = t.length; a--;)
                                i = o.indexOf(t[a]),
                                    ~i && (t[a] = o.substr(i - e, 1));
                            return t.join("")
                        },
                        s: function (t, e) {
                            var i = t.length;
                            if (i) {
                                var o = z(t, e)
                                    , a = 0;
                                for (t = t.split(""); ++a < i;)
                                    t[a] = t.splice(o[i - 1 - a], 1, t[a])[0];
                                t = t.join("")
                            }
                            return t
                        },
                        i: function (t, e) {
                            return r.s(t, e ^ vk_id)
                        },
                        x: function (t, e) {
                            var i = [];
                            return e = e.charCodeAt(0),
                                (function () {
                                    var tt = t.split("");
                                    for (var z = 0; z < tt.length; z++) {
                                        i.push(String.fromCharCode(tt[z].charCodeAt(0) ^ e))
                                    }
                                })(),
                                i.join("")
                        }
                    };

                    if (~t.indexOf("audio_api_unavailable")) {
                        var e = t.split("?extra=")[1].split("#"),
                            o = "" === e[1] ? "" : a(e[1]);
                        if (e = a(e[0]),
                            "string" != typeof o || !e)
                            return t;
                        o = o ? o.split(String.fromCharCode(9)) : [];
                        for (var p, l, n = o.length; n--;) {
                            if (l = o[n].split(String.fromCharCode(11)),
                                p = l.splice(0, 1, e)[0],
                                !r[p])
                                return t;
                            e = r[p].apply(null, l)
                        }
                        if (e && "http" === e.substr(0, 4))
                            return e
                    }
                    return t
                }

                var decryptedData = [];

                // get audio information
                for (var i = 0; i < audioDataObj.length; i++) {
                    var audioMaskedSource = audioDataObj[i][2];
                    var songName = audioDataObj[i][3];
                    var performerName = audioDataObj[i][4];
                    var audioSource = _audio_unmask_source(audioMaskedSource, userId);

                    decryptedData.push({
                        "performer": performerName,
                        "name": songName,
                        "source": audioSource,
                        "length": audioDataObj[i][5],
                        "fullId": audioDataObj[i][1] + "_" + audioDataObj[i][0]
                    });
                }

                return decryptedData;
            }

            function preloadAudioData(ids, userId) {
                return new Promise(function (resolve, reject) {
                    VkTune.helpers.makeRequest({ // request audio data from Vk API
                        url: "https://vk.com/al_audio.php",
                        method: "POST",
                        data: "act=reload_audio&al=1&ids=" + ids.join(),
                        headers: [{ "Content-type": "application/x-www-form-urlencoded" }]
                    }).then(function (response) {
                        // format received data and convert to object
                        var audioDataObj = parseApiResponse(response.body);
                        if (audioDataObj && audioDataObj.length) {
                            var decryptedAudioData = _decryptAudioData(audioDataObj, userId);
                            // TODO: log undecrypted event
                            resolve(decryptedAudioData);
                        } else {
                            chrome.runtime.sendMessage({ name: "error", value: { action: "PreloadAudio", label: "Unable to parse audio data object: ", userMessage: chrome.i18n.getMessage("errorMessageUnableToPreloadAudio") } });
                            reject();
                        }
                    }, function () {
                        // in case of network error or something
                        chrome.runtime.sendMessage({ name: "error", value: { action: "PreloadAudio", label: "Network Error. Unable to preload audio data.", userMessage: chrome.i18n.getMessage("errorMessageUnableToPreloadAudioNetworkError") } });
                    });
                });

            }

            var notPreloadedBefore = filterPreloadedAudios(audioIds);
            notPreloadedBefore.forEach(function (val) { audioInfo[val] = null; }); // replace undefined with null to mark them as loading now

            var loadingQueue = [];
            for (var i = 0; i < Math.ceil(notPreloadedBefore.length / 5); i++) {
                var lastIterationIndex = notPreloadedBefore.length < (i + 1) * 5 ? notPreloadedBefore.length : (i + 1) * 5;
                var idsToPreload = notPreloadedBefore.slice(i * 5, lastIterationIndex);
                loadingQueue.push(preloadAudioData(idsToPreload, userId));
            }

            return new Promise(function (resolve, reject) {
                Promise.all(loadingQueue).then(function (data) {
                    var allReceivedData = [];
                    data.forEach(function (d) { allReceivedData = allReceivedData.concat(d); });

                    allReceivedData.forEach(function (d) { audioInfo[d.fullId] = d; });

                    resolve(audioIds.map(function (v) { return audioInfo[v]; }));
                }, function () {
                    reject();
                });
            });
        }

        function getAudioBitrate(audiosToCalculate, audiosThatMayBeAlsoPreloaded, userId) {
            var audiosToPreload = filterPreloadedAudios(audiosToCalculate); // filter audios that already preloaded
            if (audiosToPreload.length) { // if we have to calc data for some unknown audios
                audiosToPreload = audiosToPreload.concat(audiosThatMayBeAlsoPreloaded); // append them with some extra ids
            } else {
                audiosToPreload = audiosToCalculate;
            }

            function requestFileSize(audioData) {
                return new Promise(function (resolve, reject) {
                    VkTune.helpers.makeRequest({
                        url: audioData.source,
                        method: "HEAD",
                        headers: [{ "Content-type": "application/x-www-form-urlencoded" }]
                    }).then(function (response) {
                        var headers = response.headers.match(/content-length: (\d*)/);
                        if (headers) {
                            var fileSize = Math.round(headers[1] / 1024 / 1024 * 100) / 100;
                            var bitrate = Math.round(headers[1] * 8 / audioData["length"] / 1024);
                            resolve({ bitrate: bitrate, filesize: fileSize, fullId: audioData.fullId });
                        }
                    }, function () { reject(); });
                });
            }

            return new Promise(function (resolve, reject) {
                preloadAudios(audiosToPreload, userId)
                    .then(function (audioData) {
                        var loadingQueue = [];
                        for (var i = 0; i < audiosToCalculate.length; i++) {
                            var data = audioData.find(function (e) { return e != null && e.fullId == audiosToCalculate[i] });
                            if (data && !data.filesize && !data.bitrate) {
                                loadingQueue.push(requestFileSize(data));
                            }
                        }

                        Promise.all(loadingQueue).then(function (data) {
                            var allReceivedData = [];
                            data.forEach(function (d) { allReceivedData = allReceivedData.concat(d); });

                            allReceivedData.forEach(function (info) {
                                if (audioInfo[info.fullId]) {
                                    audioInfo[info.fullId].bitrate = info.bitrate;
                                    audioInfo[info.fullId].filesize = info.filesize;
                                }
                            });

                            resolve(audiosToCalculate.map(function (v) { return audioInfo[v]; }));
                        });
                    }, function () {
                        reject();
                    });
            });
        }

        function downloadAudios(audiosToDownload, audiosThatMayBeAlsoPreloaded, userId, title, cover) {
            var audiosToPreload = filterPreloadedAudios(audiosToDownload); // filter audios that already preloaded
            if (audiosToPreload.length) { // if we have to downloads some unknown audios
                audiosToPreload = audiosToPreload.concat(audiosThatMayBeAlsoPreloaded); // append them with some extra ids
            } else {
                audiosToPreload = audiosToDownload;
            }

            return new Promise(function (resolve, reject) {
                preloadAudios(audiosToPreload, userId)
                    .then(function (audioData) {
                        var downloadData = audiosToDownload.map(function (v) { return audioData.find(function (d) { return d != null && d.fullId == v; }); });
                        resolve({ title: title, cover: cover, audios: downloadData });
                    });
            });
        }

        function parseApiResponse(responseBody) {
            var responseData = responseBody.split('<!>');
            var dataThatNeedToBeParsed = responseData[5];
            dataThatNeedToBeParsed = dataThatNeedToBeParsed.replace("<!json>", "");
            var parsedDataObj = JSON.parse(dataThatNeedToBeParsed);
            return parsedDataObj;
        }

        function downloadPlaylist(playlistFullId, userId) {
            var playlistData = playlistFullId.split('_');

            return new Promise(function (resolve, reject) {
                VkTune.helpers.makeRequest({ // request playlist data from Vk API
                    url: "https://vk.com/al_audio.php",
                    method: "POST",
                    data: "act=load_section&al=1&offset=0&is_loading_all=1&claim=0&type=" + playlistData[0] + "&owner_id=" + playlistData[1] + "&playlist_id=" + playlistData[2] + "&access_hash=",
                    headers: [{ "Content-type": "application/x-www-form-urlencoded" }]
                }).then(function (responseData) {
                    var playlistDataObj = parseApiResponse(responseData.body);

                    if (playlistDataObj && playlistDataObj["list"]) {
                        var audiosToDownload = playlistDataObj["list"].map(function (lstObj) { return lstObj[1] + "_" + lstObj[0]; })

                        downloadAudios(audiosToDownload, [], userId, playlistDataObj["title"], playlistDataObj["coverUrl"])
                            .then(function (downloadData) {
                                resolve(downloadData);
                            });
                    } else {
                        // Unable to parse playlist data
                        chrome.runtime.sendMessage({ name: "error", value: { action: "GetPlaylist", label: "Unable to parse playlist data.", userMessage: chrome.i18n.getMessage("errorMessageUnableToGetPlaylist") } });
                        reject();
                    }
                    }, function () {
                        // Unable to get playlist data
                        chrome.runtime.sendMessage({ name: "error", value: { action: "GetPlaylist", label: "Network Error. Unable to get playlist data.", userMessage: chrome.i18n.getMessage("errorMessageUnableToGetPlaylistNetworkError") } });
                        reject();
                    });
            });
        }

        //TODO: move classes to constants
        function getAudioInfoElement(bitrate, weight) {
            var infoWrapper = document.createElement("span");
            infoWrapper.className = "_vktune_audio_info";
            var bitrateElement = document.createElement("span");
            bitrateElement.className = "_vktune_bitrate";
            bitrateElement.textContent = bitrate + "kbps";
            infoWrapper.appendChild(bitrateElement);
            var weightElement = document.createElement("span");
            weightElement.className = "_vktune_weight";
            weightElement.textContent = weight + "Mb";
            infoWrapper.appendChild(weightElement);
            return infoWrapper;
        }

        return {
            getAudioBitrate: getAudioBitrate,
            downloadAudios: downloadAudios,
            getAudioInfoElement: getAudioInfoElement,
            downloadPlaylist: downloadPlaylist
        }
    })();
})()