(function () {

    if (!window.VkTune)
        window.VkTune = {};

    VkTune.constants = (function () {
        return {
            //keys
            settingsStorageKey: "settings",
            showActionKey: 'show',
            hideActionKey: 'hide',
            markActionKey: 'mark',
            //selectors
            leftAdsBlockSelector: '#ads_left',                      // selector of left-box ads container
            promotedPostsSelector: '._ads_promoted_post_data_w',    // selector of advert wall posts
            groupAdsSelector: '.wall_marked_as_ads',                // selector of gear in some posts that were promoted by group admins
            //classes
            postClass: '_post',
            vktuneMarkedClass: 'vktune_marked'
        }
    })();

    VkTune.helpers = (function () {
        // Jquery's analog of extend function
        var extend = function (out) {
            out = out || {};

            for (var i = 1; i < arguments.length; i++) {
                if (!arguments[i])
                    continue;

                for (var key in arguments[i]) {
                    if (arguments[i].hasOwnProperty(key))
                        out[key] = arguments[i][key];
                }
            }

            return out;
        };

        // Jquery's analog of hasClass function
        var hasClass = function (element, className) {
            return (" " + element.className.toLowerCase() + " ").indexOf(" " + className.toLowerCase() + " ") > -1;
        }

        // Removes element from DOM
        var removeNode = function (node) {
            node.parentNode.removeChild(node);
        }

        var nextSibling = function (node, className) {
            var possibleNode = node.nextSibling;
            while (possibleNode && possibleNode.nodeType != 1) {
                possibleNode = possibleNode.nextSibling;
            }
            if (possibleNode && (!className || hasClass(possibleNode, className))) {
                return possibleNode;
            }
            return null;
        }

        // Jquery's analog of parent function
        var selfOrParent = function (element, className) {
            var temp = element;
            if (className) {
                while (!hasClass(temp, className)) { // pop-up to post
                    temp = temp.parentNode;
                }
            } else {
                temp = temp.parentNode;
            }            
            return temp;
        }

        // XHR request wrapped with a promise
        var makeRequest = function (requestParams) {
            var defaultParams = {
                url: "/",
                method: "GET",
                data: null,
                headers: []
            }
            var params = extend({}, defaultParams, requestParams);

            return new Promise(function (resolve, reject) {
                var xhr = new XMLHttpRequest();

                xhr.open(params.method, params.url, true);
                for (var i = 0; i < params.headers.length; i++) {
                    for (var key in params.headers[i]) {
                        if (params.headers[i].hasOwnProperty(key))
                            xhr.setRequestHeader(key, params.headers[i][key]);
                    }
                }
                xhr.onload = function () {
                    if (this.status == 200) {
                        resolve({
                            body: this.response,
                            headers: this.getAllResponseHeaders()
                        });
                    } else {
                        var error = new Error(this.statusText);
                        error.code = this.status;
                        reject(error);
                    }
                };
                xhr.onerror = function () {
                    reject(new Error("Network Error"));
                };
                xhr.send(params.data);
            });
        }

        // function to replace non-ascii symbols which presented as "&#00000;"
        var unicodeStringFromEscaped = function (escapedString) {
            var escapedCharRE = /&#(\d*);/;

            while (escapedCharRE.test(escapedString)) {
                var symbolCode = escapedString.match(escapedCharRE)[1]
                var originalSymbol = String.fromCharCode(symbolCode);
                escapedString = escapedString.replace(escapedCharRE, originalSymbol);
            }

            escapedString = escapedString.replace(/&amp;/g, '&');
            escapedString = escapedString.replace(/&quot;/g, '"');

            return escapedString;
        }

        // Function to replace \/:*?"<>| symbols as its unable to save file with them in OS
        var replaceForbiddenSymbols = function (filename) {
            var re = /[\\\/\:\*\?\"\<\>\|]/g;
            return filename.replace(re, "_");
        }

        // Filename format helper
        var getFileName = function (performer, songName, fileNameTemplate) {
            performer = unicodeStringFromEscaped(performer);
            performer = replaceForbiddenSymbols(performer);
            performer = performer.trim();
            songName = unicodeStringFromEscaped(songName);
            songName = replaceForbiddenSymbols(songName);
            songName = songName.trim();
            
            fileNameTemplate = fileNameTemplate.replace(/#p/g, performer);
            fileNameTemplate = fileNameTemplate.replace(/#s/g, songName);
            fileNameTemplate += ".mp3";

            return fileNameTemplate;
        }

        getFilePathPrefix = function (rootFolderName, albumName) {
            var result = "";

            if (rootFolderName) {
                rootFolderName = unicodeStringFromEscaped(rootFolderName);
                rootFolderName = replaceForbiddenSymbols(rootFolderName);
                rootFolderName = rootFolderName.trim();
                if (rootFolderName) {
                    result += rootFolderName + "/"
                }
            }
            if (albumName) {
                albumName = unicodeStringFromEscaped(albumName);
                albumName = replaceForbiddenSymbols(albumName);
                albumName = albumName.trim();
                if (albumName) {
                    result += albumName + "/"
                }
            }

            return result;
        }
        
        return {
            extend: extend,
            hasClass: hasClass,
            removeNode: removeNode,
            selfOrParent: selfOrParent,
            makeRequest: makeRequest,
            getFileName: getFileName,
            getFilePathPrefix: getFilePathPrefix,
            nextSibling: nextSibling
        }
    })();

    VkTune.settings = (function () {
        var defaultSettings = {     // settings defaults when user didn't select something
            'app.licenseKey': '',
            'audio.downloading': true,                              // true||false
            'audio.calcBitrate': false,                             // true||false
            'audio.downloadRootFolderName': "VkTune",               // VkTune
            'audio.downloadMethod': "saveTo",                       // saveTo||saveAs
            'audio.downloadConflictAction': "uniquify",             // overwrite||uniquify
            'audio.downloadPathMask': "#p - #s",
            'audio.playlistsDownloading': true,                     // true||false
            'audio.savePositionsInPlaylists': true,                 // true||false
            'ads.mainBlock': VkTune.constants['markActionKey'],     // show||mark||hide
            'ads.promotedPosts': VkTune.constants['markActionKey'], // show||mark||hide
            'ads.groupWallAds': VkTune.constants['markActionKey']   // show||mark||hide
        }

        // local function to get settings which was manually set by user
        var getUserSettings = function () {
            return new Promise(function (resolve, reject) {
                chrome.storage.sync.get(VkTune.constants["settingsStorageKey"], function (_userSettings) {
                    var storedSettingsItem = _userSettings[VkTune.constants["settingsStorageKey"]];
                    var userSettings = {};
                    if (storedSettingsItem) {
                        userSettings = storedSettingsItem
                    }
                    resolve(userSettings);
                });
            });
        }

        // function that returns settings object
        var getSettingsItem = function () {
            return new Promise(function (resolve, reject) {
                getUserSettings()
                    .then(function (_userSettings) {
                        var settings = VkTune.helpers.extend({}, defaultSettings, _userSettings); // extend manual settings with defaults and 

                        settings['app.licenseLevel'] = (function (licenseKey) {
                            function hashCode(s) { return s.split("").reduce(function (a, b) { a = ((a << 5) - a) + b.charCodeAt(0); return a & a }, 0); }
                            var accessHashes = { "995560693": 1, "1383352002": 2 }
                            var hashedKey = hashCode(licenseKey).toString();
                            return accessHashes[hashedKey] ? accessHashes[hashedKey] : 0;
                        })(settings['app.licenseKey']);

                        resolve(settings);
                    });
            });
        }

        // return single property
        var getValue = function (key) {
            return new Promise(function (resolve, reject) {
                getSettingsItem() // get settings
                    .then(function (_settings) {
                        resolve(_settings[key]);
                    });
            });
        }

        // set single property
        var setValue = function (key, value) {
            return new Promise(function (resolve, reject) {
                if (key == 'app.licenseLevel') { reject(); }
                getUserSettings()
                    .then(function (_userSettings) {
                        _userSettings[key] = value; // fill user settings with new value
                        var storageItem = {};
                        storageItem[VkTune.constants["settingsStorageKey"]] = _userSettings;

                        chrome.storage.sync.set(storageItem, function () { // save user settings to browser.storage
                            getSettingsItem()
                                .then(function (_settings) {
                                    resolve(_settings);
                                });
                        });
                    });
            });
        }

        // restore settings - actually just deleting storage object
        var restore = function () {
            return new Promise(function (resolve, reject) {
                getValue('app.licenseKey')
                    .then(function (licenseKey) {
                        chrome.storage.sync.remove(VkTune.constants["settingsStorageKey"], function () {
                            setValue('app.licenseKey', licenseKey)
                                .then(function (_settings) {
                                    resolve(_settings);
                                });
                        });
                    });
            });
        }

        // passing functions to make them public
        return {
            defaultSettings: (function () { return VkTune.helpers.extend({}, defaultSettings); })(),
            getAll: getSettingsItem,
            getValue: getValue,
            setValue: setValue,
            restore: restore
        }
    })();

})();