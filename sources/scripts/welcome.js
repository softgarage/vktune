var showContentForLicense = function (licenseLevel) {
    var variableContent = document.querySelectorAll("[visible-for-license-level]");
    for (var i = 0; i < variableContent.length; i++) {
        variableContent[i].classList.add("hidden");
    }

    variableContent = document.querySelectorAll("[visible-for-license-level='" + licenseLevel + "']");
    for (var i = 0; i < variableContent.length; i++) {
        variableContent[i].classList.remove("hidden");
    }

    variableContent = document.querySelectorAll("[visible-for-license-level-and-more]");
    for (var i = 0; i < variableContent.length; i++) {
        if (parseInt(licenseLevel) >= parseInt(variableContent[i].getAttribute("visible-for-license-level-and-more"))) {
            variableContent[i].classList.remove("hidden");
        } else {
            variableContent[i].classList.add("hidden");
        }
    }
};

// Init payform
(function () {
    var iframe = document.querySelector("#payform iframe");
    if (iframe) {
        var urlTemplate = "https://money.yandex.ru/quickpay/shop-widget?writer=seller&targets=%D0%91%D0%BB%D0%B0%D0%B3%D0%BE%D0%B4%D0%B0%D1%80%D0%BD%D0%BE%D1%81%D1%82%D1%8C%20%D0%B7%D0%B0%20%D0%B8%D1%81%D0%BF%D0%BE%D0%BB%D1%8C%D0%B7%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5&targets-hint=&default-sum=100&button-text=13&payment-type-choice=on&hint=&successURL={REDIRECT_URL}%3Fthankyou%3Drosebud&quickpay=shop&account=41001949197627";
        var thisPageUrl = window.location.origin + window.location.pathname;
        var thisPageEscapedUrl = thisPageUrl.replace(/:/g, "%3A").replace(/\//g, "%2F");
        var url = urlTemplate.replace("{REDIRECT_URL}", thisPageEscapedUrl)
        iframe.setAttribute("src", url);
    }
})();

// Handling options button click
var optionsCTAs = document.querySelectorAll(".options-cta");
for (var i = 0; i < optionsCTAs.length; i++) {
    optionsCTAs[i].addEventListener('click', function (e) { chrome.runtime.openOptionsPage(); });
}

// Show appropriate content for license level
(function () {
    VkTune.settings.getAll()
        .then(function (settings) {
            var level = settings['app.licenseLevel'] ? settings['app.licenseLevel'] : 0;
            showContentForLicense(level);
        });
})();

// Handle thankyou page
(function () {
    var codeMatch = window.location.search.match(/thankyou=([^&]*)/);
    if (codeMatch) { // if page contains thankyou in query
        var licenseCode = codeMatch[1];
        VkTune.settings.setValue('app.licenseKey', licenseCode)
            .then(function (settings) {
                var level = settings['app.licenseLevel'] ? settings['app.licenseLevel'] : 0;
                showContentForLicense(level);

                document.getElementById("payform").classList.add("hidden");
                document.getElementById("thankyouBlock").classList.remove("hidden");
                document.getElementById("licenseText").textContent = licenseCode;
            });
    }
})();