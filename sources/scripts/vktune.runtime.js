(function () {

    if (!window.VkTune)
        window.VkTune = {};

    VkTune.runtime = (function () {

        var onInstalled = function () {
            //VkTune.notifications.show("extension.installed", "VkTune", "Thanks for choosing VkTune. Hope, you will like this experience.", null);
            VkTune.analytics.logAppInstalled("");

            chrome.tabs.create({ url: chrome.extension.getURL('pages/welcome.html') });
        }

        var onUpdated = function (previousVersion) {
            VkTune.notifications.show("VkTune.updated", chrome.i18n.getMessage("extensionUpdatedNotificationTitle"), chrome.i18n.getMessage("extensionUpdatedNotificationMessage"), null);
            VkTune.analytics.logAppUpdated(previousVersion);
            //chrome.tabs.create({ url: chrome.extension.getURL('pages/welcome.html') });
        }

        var onUpdateAvailable = function (newVersion) {
            VkTune.notifications.show("VkTune.updateAvailable", chrome.i18n.getMessage("updateAvailableNotificationTitle", newVersion), chrome.i18n.getMessage("updateAvailableNotificationMessage"), null);
        }

        var onAudioDownload = function (downloadData) {

            VkTune.settings.getAll()
                .then(function (settings) {
                    var containsMultipleFiles = downloadData.audios.length > 1;
                    var pathTemplate = settings['audio.downloadPathMask'];
                    var promptSaveDialog = settings['audio.downloadMethod'] == "saveAs" && !containsMultipleFiles;
                    if (promptSaveDialog) {
                        pathTemplate = VkTune.settings.defaultSettings['audio.downloadPathMask'];
                    }
                    var addIndexToFileName = containsMultipleFiles && settings['audio.savePositionsInPlaylists'];
                    var downloadingPath = VkTune.helpers.getFilePathPrefix(settings['audio.downloadRootFolderName'], downloadData.title);

                    for (var i = 0; i < downloadData.audios.length; i++) {
                        var audio = downloadData.audios[i];
                        if (audio) {
                            var filename = VkTune.helpers.getFileName(audio.performer, audio.name, pathTemplate);

                            if (addIndexToFileName) { // add Index to filename if needed
                                var indexText = (i + 1) + ". ";
                                var indexPosition = filename.lastIndexOf('/') + 1;
                                filename = [filename.slice(0, indexPosition), indexText, filename.slice(indexPosition)].join('');
                            }

                            var fullDownloadingPath = downloadingPath + filename;
                            var downloadOptions = {};
                            downloadOptions["url"] = audio.source;
                            downloadOptions["filename"] = fullDownloadingPath
                            downloadOptions["conflictAction"] = settings['audio.downloadConflictAction'];
                            downloadOptions["saveAs"] = promptSaveDialog;

                            // start downloading from received URL
                            chrome.downloads.download(downloadOptions);
                        }
                    }

                    if (containsMultipleFiles) {
                        var notNullDatas = downloadData.audios.filter(function (d) { return d; });
                        VkTune.analytics.trackPlaylistDownload(downloadData.title, downloadData.audios.length, notNullDatas.length);
                    } else {
                        VkTune.analytics.trackAudioDownload(fullDownloadingPath);
                    }

                    // download album cover if needed
                    if (downloadData.cover) {
                        var coverPath = downloadingPath + "/" + "cover.jpg"
                        chrome.downloads.download({
                            "url": downloadData.cover,
                            "filename": coverPath,
                            "conflictAction": "overwrite",
                            "saveAs": false
                        });
                    }
                });
        }

        var onMainAdvertismentHidden = function (data) {
            VkTune.analytics.trackAdsHidden(data.method, data.count);
        }

        var onPromotedPostHide = function (data) {
            VkTune.analytics.trackPromoPostsHidden(data.method, data.count);
        }

        var onGroupAdvertismentHide = function (data) {
            VkTune.analytics.trackPaidPostsHidden(data.method, data.count);
        }

        return {
            onInstalled: onInstalled,
            onUpdated: onUpdated,
            onUpdateAvailable: onUpdateAvailable,
            onAudioDownload: onAudioDownload,
            onMainAdvertismentHidden: onMainAdvertismentHidden,
            onPromotedPostHide: onPromotedPostHide,
            onGroupAdvertismentHide: onGroupAdvertismentHide
        }
    })();

    VkTune.notifications = (function(){

        function createNotification(id, title, message, iconUrl) {
            if (!title || !message) {
                return;
            }
            var notificationOptions = { };
            notificationOptions["type"] = "basic";
            notificationOptions["title"] = title;
            notificationOptions["message"] = message;

            if (!iconUrl) iconUrl = "images/96.png";
            notificationOptions["iconUrl"] = chrome.extension.getURL(iconUrl);

            chrome.notifications.create(id, notificationOptions);
        }

        function errorNotification(id, title, message) {
            createNotification(id, title, message, "images/error-96.png");
        }

        function baseNotification(id, title, message, iconUrl) {
            createNotification(id, title, message, iconUrl);
        }

        return {
            error: errorNotification,
            show: baseNotification
        }
        
    })();
})();