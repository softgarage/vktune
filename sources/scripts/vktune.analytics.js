(function () {

    if (!window.VkTune)
        window.VkTune = {};

    // As analytics may be used only by background script it was separated to a single file and appended with manifest
    VkTune.analytics = (function () {
        var GA = null; // google analytics object
        var counters = null;

        var countersStorageKey = "counters";

        var audioDownloadCounterName = "audioDownload";
        var adsHiddenCounterName = "adsHidden";
        var promoPostsHiddenCounterName = "promoPostsHidden";
        var paidPostsHiddenCounterName = "paidPostsHidden";
        var usageStartCounterName = "usageStart";

        // function that returns addon's counters
        var getCounters = function () {
            return new Promise(function (resolve, reject) {
                if (!counters) {
                    counters = {};
                    counters[audioDownloadCounterName] = 0;
                    counters[adsHiddenCounterName] = 0;
                    counters[promoPostsHiddenCounterName] = 0;
                    counters[paidPostsHiddenCounterName] = 0;
                    counters[usageStartCounterName] = Date.now();

                    chrome.storage.sync.get(countersStorageKey, function (_counters) {
                        if (_counters && _counters[countersStorageKey]) {
                            counters = VkTune.helpers.extend({}, counters, _counters[countersStorageKey]);
                        }

                        resolve(counters);
                    });
                } else {
                    resolve(counters);
                }
            });
        }

        // function that stores counters object in browser.storage
        var setCounters = function(_counters) {
            return new Promise(function(resolve, reject) {
                var storageItem = {};
                storageItem[countersStorageKey] = _counters;

                chrome.storage.sync.set(storageItem,  function () {
                    resolve();
                });
            });
        }

        // increment counter (on event firing)
        var incCounter = function (counterName, incValue) {
            return new Promise(function (resolve, reject) {
                getCounters()
                    .then(function (_counters) {
                        var newVal = (_counters[counterName] ? _counters[counterName] : 0) + (incValue ? incValue : 1);
                        _counters[counterName] = newVal;
                        setCounters(_counters)
                            .then(function () {
                                resolve(newVal);
                            });                        
                    });
            });
        }

        //reset counters to 0's
        var resetCounters = function() {
            return new Promise(function (resolve, reject) {
                counters = null;
                chrome.storage.sync.remove(countersStorageKey, function() {
                    resolve();
                })
            });
        }


        var trackAudioDownload = function(audioName) {
            incCounter(audioDownloadCounterName).then(function () {
                if (GA) {
                    GA.Event("Audio", "Download", audioName);
                }
            });
        }
        var trackPlaylistDownload = function (playlistName, totalRecordsCount, recordsCount) {
            incCounter(audioDownloadCounterName, recordsCount).then(function () {
                if (GA) {
                    GA.Event("Audio", "PlaylistDownload", "Name: '" + playlistName + "'; Total: " + totalRecordsCount + "; Downloaded: " + recordsCount);
                }
            });
        }
        var trackAdsHidden = function (method, count) {
            incCounter(adsHiddenCounterName, count)
                .then(function (counterValue) {
                    if (GA) {
                        for (var i = 0; i < count; i++) {
                            GA.Event("Ads", adsHiddenCounterName, method);
                        }
                    }
                });
        }
        var trackPromoPostsHidden = function (method, count) {
            incCounter(promoPostsHiddenCounterName, count)
                .then(function (counterValue) {
                    if (GA) {
                        for (var i = 0; i < count; i++) {
                            GA.Event("Ads", promoPostsHiddenCounterName, method);
                        }
                    }
                });
        }
        var trackPaidPostsHidden = function (method, count) {
            incCounter(paidPostsHiddenCounterName, count)
                .then(function (counterValue) {
                    if (GA) {
                        for (var i = 0; i < count; i++) {
                            GA.Event("Ads", paidPostsHiddenCounterName, method);
                        }
                    }
                });
        }
        var logError = function(errorType, errorData) {
            if (GA) {
                GA.Event("Error", errorType, errorData);
            }
        }

        //if user allowed to track events with google analytics - enable it
        var initGA = function (uID, uLang) {
            if (GA) {
                return;
            }

            GA = (function (userID, lang) {
                var createAnalyticsMessage = function (trackingId, userId, lang) {
                    var m = {
                        "v": "1",
                        "tid": trackingId,
                        "cid": userId,
                        "uid": userId,
                        "aip": "1",
                        "ds": "add-on",
                        serialize: function () {
                            var str = [];
                            for (var p in this)
                                if (this.hasOwnProperty(p) && typeof (this[p]) != "function") {
                                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(this[p]));
                                }
                            return str.join("&");
                        }
                    };
                    if (lang) {
                        m["ul"] = lang;
                    }
                    return m;
                }

                return {
                    GA_SERVICE_URL: "https://www.google-analytics.com/collect",
                    GA_TRACKING_ID: "UA-107837851-3",
                    GA_USER_ID: userID,
                    GA_USER_LANG: lang,

                    // function to track event with GA
                    Event: function (eCat, eAct, eParam) {
                        try {
                            var request = new XMLHttpRequest();
                            var message = createAnalyticsMessage(this.GA_TRACKING_ID, this.GA_USER_ID, this.GA_USER_LANG);
                            message.t = "event";
                            message.ec = eCat;
                            message.ea = eAct;
                            message.el = eParam;

                            request.open("POST", this.GA_SERVICE_URL, true);
                            request.send(message.serialize());
                        } catch (e) {
                            //this._log("Error sending report to Google Analytics.\n" + e);
                        }
                    }
                }

            })(uID);

            GA.Event("User", "Init", GA.GA_USER_ID, uLang);
        }

        var logAppInstalled = function(data) {
            if (GA) {
                GA.Event("App", "Installed", data);
            }
        }

        var logAppUpdated = function(data) {
            if (GA) {
                GA.Event("App", "Updated", data);
            }
        }

        // call this in case of user prohibited to send data to GA
        var terminateGA = function () {
            if (GA) GA = null;
        }

        return {
            initGA: initGA,
            trackAudioDownload: trackAudioDownload,         // function to track audio downloading event
            trackPlaylistDownload: trackPlaylistDownload,   // function to track audio playlist downloading event
            trackAdsHidden: trackAdsHidden,                 // function to track ads hide event
            trackPromoPostsHidden: trackPromoPostsHidden,   // function to track promo post hide event
            trackPaidPostsHidden: trackPaidPostsHidden,     // function to track paid post hide event
            getCounters: getCounters,                       // function to receive stats on options page
            resetCounters: resetCounters,                   // function to reset stats
            logError: logError,
            logAppInstalled: logAppInstalled,
            logAppUpdated: logAppUpdated
        }
    })();
})()