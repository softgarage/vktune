// Options.js - script file for addon's settings page

function saveOption(e) {
    var key = e.target.name;
    var value = (e.target.type == "checkbox") ? e.target.checked : e.target.value;

    var relatedBlocks = document.querySelectorAll("[visible-if-checked='" + key + "']");
    relatedBlocks.forEach(function (block) {
        block.style.display = value ? "block" : "none";
    });

    VkTune.settings.setValue(key, value);
    e.preventDefault();
}

function restoreOptions() {
    VkTune.settings.getAll()
        .then(function (settings) {
            for (var setting in settings) {
                if (settings.hasOwnProperty(setting)) {
                    var settingUi = document.querySelector("[name='" + setting + "']");
                    if (settingUi) {
                        if (settingUi.type == "checkbox") {
                            settingUi.checked = settings[setting] ? "checked" : "";
                            var relatedBlocks = document.querySelectorAll("[visible-if-checked='" + setting + "']");
                            relatedBlocks.forEach(function (block) {
                                block.style.display = settings[setting] ? "block" : "none"; 
                            });
                        } else
                        if (settingUi.type == "radio") {
                            settingUi = document.querySelector("[name='" + setting + "'][value='" + settings[setting] + "']");
                            settingUi.checked = true;
                        } else {
                            settingUi.value = settings[setting];
                        }
                    }
                }
            }

            // set base downloading folder
            var downloadRootFolderName = document.getElementById("downloadRootFolderName");
            downloadRootFolderName.textContent = settings['audio.downloadRootFolderName'];

            // set license key
            var licenseKeyElement = document.getElementById('licenseKey');
            if (settings['app.licenseLevel']) {
                licenseKeyElement.textContent = settings['app.licenseKey'];
            } else {
                licenseKeyElement.textContent = chrome.i18n.getMessage("noLicenseText");
            }

            // hide licensed blocks
            var licensedBlocks = document.querySelectorAll("[enabled-on-license-level]");
            licensedBlocks.forEach(function (element) {
                var unavailableTitleText = chrome.i18n.getMessage("appLicenseUnavailableOption");
                if (element.tagName == "DIV" || element.tagName == "SPAN") {
                    element.parentElement.setAttribute('title', unavailableTitleText);
                    element.classList.add('disabled');
                } else {
                    element.setAttribute('title', unavailableTitleText);
                    element.setAttribute('disabled', 'disabled');
                }
            });
            for (var level = 0; level <= settings['app.licenseLevel']; level++) {
                var thisLevelBlocks = document.querySelectorAll("[enabled-on-license-level='" + level + "']");
                thisLevelBlocks.forEach(function (element) {
                    if (element.tagName == "DIV" || element.tagName == "SPAN") {
                        element.parentElement.removeAttribute('title');
                        element.classList.remove('disabled');
                    } else {
                        element.removeAttribute("title");
                        element.removeAttribute('disabled');
                    }
                });
            }
        });
}

function restoreStats() {
    VkTune.analytics.getCounters()
        .then(function(stats) {
            var usageInterval = (Date.now() - stats["usageStart"]) / 1000;
            var daysUsageInterval = Math.floor(usageInterval / 86400);
            if (daysUsageInterval > 0) {
                document.querySelector("#stats-period").innerText = daysUsageInterval + " д";
            } else {
                var hoursUsageInterval = Math.floor(usageInterval / 3600);
                if (hoursUsageInterval > 0) {
                    document.querySelector("#stats-period").innerText = hoursUsageInterval + " ч";
                } else {
                    var minsUsageInterval = Math.floor(usageInterval / 60);
                    if (minsUsageInterval > 0) {
                        document.querySelector("#stats-period").innerText = minsUsageInterval + " мин";
                    } else {
                        var secsUsageInterval = Math.floor(usageInterval);
                        if (secsUsageInterval > 0) {
                        document.querySelector("#stats-period").innerText = secsUsageInterval + " сек";
                        } else {
                            document.querySelector("#stats").style.display = "none";
                        }
                    }
                }
            }
            

            for (var stat in stats) {
                if (stats.hasOwnProperty(stat)) {
                    statUi = document.querySelector("#" + stat);
                    if (statUi) {
                        statUi.innerText = stats[stat];
                    }
                }
            }
        });
}

function resetDefaults() {
    VkTune.settings.restore()
        .then(function () {
            restoreOptions();
        });
}

function resetStats() {
    VkTune.analytics.resetCounters();
    restoreStats();
 }


document.addEventListener('DOMContentLoaded', function() { restoreOptions(); restoreStats(); });
document.querySelector("#settingsResetCTA").addEventListener('click', resetDefaults);
//document.querySelector("#stats").addEventListener('click', resetStats);
var settingsInputs = document.querySelectorAll("input,select");
for (var i = 0; i < settingsInputs.length; i++) {
    settingsInputs[i].addEventListener("change", saveOption);
}

// append icon to downloadsFolder
(function() {
    var styleElement = document.createElement("style");
    var downloadFolderIconStyle = "#downloadsFolder:before{background: url('" + chrome.runtime.getURL('images/folder.png') + "') left center no-repeat;content: ' '; width: 19px; height: 14px;display: inline-block;vertical-align: top;background-size: contain;}"
    styleElement.innerText = downloadFolderIconStyle; // filling style element with generated css
    document.querySelector("head").appendChild(styleElement); 

    // append click handler to downloadsFolder
    document.getElementById("downloadsFolder").addEventListener('click', function() { chrome.downloads.showDefaultFolder(); });
})();

// Translate page texts
(function () {
    // Translate text nodes
    var internationalTextItems = document.querySelectorAll(".international-content");
    internationalTextItems.forEach(function (item) {
        var content = chrome.i18n.getMessage(item.id);
        if (content) {
            item.textContent = content;
        }
    });
    // Translate titles
    var internationalTitleItems = document.querySelectorAll(".international-title");
    internationalTitleItems.forEach(function (item) {
        var content = chrome.i18n.getMessage(item.id);
        if (content) {
            item.setAttribute("title", content);
        }
    });
})();

// downloadRootFolderCTA
(function () {
    var downloadFolderCTAElement = document.getElementById('downloadRootFolderNameWrapper');
    downloadFolderCTAElement.onclick = function (e) {
        VkTune.settings.getValue('audio.downloadRootFolderName')
            .then(function (currentFolderName) {
                var downloadFolderNameInput = document.getElementById("downloadFolderInput");
                downloadFolderNameInput.value = currentFolderName;
                $('#downloadFolderDialog').modal('show');
            });        
    };

    var downloadFolderSaveCTAElement = document.getElementById("downloadFolderSaveCTAElement");
    downloadFolderSaveCTAElement.onclick = function (e) {
        var downloadFolderNameInput = document.getElementById("downloadFolderInput");
        var newDownloadFolderName = downloadFolderNameInput.value;
        if (newDownloadFolderName != null) {
            VkTune.settings.setValue('audio.downloadRootFolderName', newDownloadFolderName)
                .then(function (settings) {
                    restoreOptions();
                });
        }
    };
})();

// license CTA
(function () {
    var licenseCTAElement = document.getElementById('licenseBtn');
    licenseCTAElement.onclick = function (e) {
        VkTune.settings.getValue('app.licenseKey')
            .then(function (currentLicenseKey) {
                var licenseKeyInput = document.getElementById("licenseKeyInput");
                licenseKeyInput.value = currentLicenseKey;
                $('#licenseDialog').modal('show');
            });
    };

    var licenseSaveCTAElement = document.getElementById("licenseSaveCTAElement");
    licenseSaveCTAElement.onclick = function (e) {
        var licenseKeyInput = document.getElementById("licenseKeyInput");
        var newLicenseKey = licenseKeyInput.value;
        if (newLicenseKey != null) {
            VkTune.settings.setValue('app.licenseKey', newLicenseKey)
                .then(function (settings) {
                    restoreOptions();
                });
        }
    };
})();

// about CTA
(function () {
    var aboutBtn = document.getElementById("aboutBtn");
    aboutBtn.onclick = function (e) {
        chrome.tabs.create({ url: chrome.extension.getURL('pages/welcome.html') });
    };
})();