VK Tune

Simple web extension that allows user to update UX.
v.0.1: Basic functionality of music downloading


Extension updates .audio_row elements across site with same way. Can be checked on public pages such as:
https://vk.com/wall-28267125_574
https://vk.com/vegetablesminsk

Privacy Policy
This extension uses Google Analytics service for stats collecting in different browsers (as it's presented in both of Mozilla and Google extension stores). It collects only public data (user id at init) and app-related data (name of downloading composition, tracking of advertisment removal event). Collected data is using for efficiency analysys of application work. If you don't want to provide any of this data it's possible to turn off tracking in extension options.

Политика Конфиденциальности
Данное дополнение использует Google Analytics для сбора статистики использования дополнения в различных браузерах (поскольку оно представлено в магазинах приложений Mozilla и Google). Для статистики собираются только публичные данные (id пользователя при входе) и данные приложения (название скачиваемой композиции, событие скрытия рекламного блока или поста). Собранные данные используются для анализа эффективности приложения. В случае несогласия со сбором этих данных возможно отключение трекинга в настройках приложения.


End-user License agreement
By installing this addon user accept that some data, presented in social network vkontakte, may be protected with copyrights and prohibited to use with some ways (in particular with downloading with this addon) by law. In case of controversial situation the responsibility for user actions rests with the user. Author(s) of this addon categorically against copyright infringement and are not responsible for user actions.

Лицензионное соглашение
Устанавливая это дополнение, пользователь соглашается с тем, что композиции, представленные в социальной сети Вконтакте, могут быть защищены авторским правом и запрещены к использованию определенным образом (в частности скачиванию с использованием данного дополнения) законом. В случае возникновения спорной ситуации ответственность за свои действия возлагается на пользователя. Автор(ы) дополнения категорически против нарушения авторских прав и не несет(ут) ответственности за действия пользователей.