var gulp = require('gulp');
var less = require('gulp-less');
var minifyCSS = require('gulp-csso');
var uglify = require('gulp-uglify');
var zip = require('gulp-zip');
var clean = require('gulp-clean');

gulp.task('clean', function () {
    return gulp.src('compiled/*.zip', {read: false})
        .pipe(clean());
});

gulp.task('images', function(){
  return gulp.src('sources/images/*')
	.pipe(gulp.dest('compiled/debug/images'))
    .pipe(gulp.dest('compiled/release/images'))
});

gulp.task('locales', function () {
    return gulp.src('sources/_locales/**/*')
        .pipe(gulp.dest('compiled/debug/_locales'))
        .pipe(gulp.dest('compiled/release/_locales'))
});

gulp.task('manifest', function(){
  return gulp.src('sources/manifest.json')
	.pipe(gulp.dest('compiled/debug'))
    .pipe(gulp.dest('compiled/release'))
});

gulp.task('pages', function(){
  return gulp.src('sources/pages/*.html')
	.pipe(gulp.dest('compiled/debug/pages'))
    .pipe(gulp.dest('compiled/release/pages'))
});

gulp.task('styles', function(){
   gulp.src('sources/styles/*.less')
    .pipe(less())
	.pipe(gulp.dest('compiled/debug/styles'))
    .pipe(minifyCSS())
    .pipe(gulp.dest('compiled/release/styles'));
   gulp.src('sources/styles/*.css')
    .pipe(gulp.dest('compiled/debug/styles'))
	.pipe(gulp.dest('compiled/release/styles'));
});

gulp.task('scripts', function(){
    gulp.src('sources/scripts/*.js')
        .pipe(gulp.dest('compiled/debug/scripts'))
        .pipe(uglify())
        .pipe(gulp.dest('compiled/release/scripts'));
   gulp.src('sources/background.js')
       .pipe(gulp.dest('compiled/debug'))
       .pipe(uglify())
       .pipe(gulp.dest('compiled/release'));
   gulp.src('sources/contentScript.js')
       .pipe(gulp.dest('compiled/debug'))
       .pipe(uglify())
       .pipe(gulp.dest('compiled/release'));
   gulp.src('sources/bootstrap.min.js')
       .pipe(gulp.dest('compiled/debug'))
       .pipe(gulp.dest('compiled/release'));
});

gulp.task('zip', function(){
    gulp.src('compiled/release/**/*')
        .pipe(zip('release.zip'))
        .pipe(gulp.dest('compiled/'));
	gulp.src('compiled/debug/**/*')
        .pipe(zip('debug.zip'))
        .pipe(gulp.dest('compiled/'));
});

gulp.task('default', [ 'clean', 'images', 'locales', 'manifest', 'pages', 'styles', 'scripts', 'zip' ]);
